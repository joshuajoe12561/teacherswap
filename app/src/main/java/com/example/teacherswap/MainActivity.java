package com.example.teacherswap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import io.paperdb.Paper;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class MainActivity extends AppCompatActivity {

    Button btnLogin;
    EditText edPhone,edtPassword;
    TextView txtSignUp;
    private String county;
    private DatabaseReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       Paper.init(this);
        btnLogin=findViewById(R.id.btnLogin_id);
        edPhone=findViewById(R.id.phone_id);
        edtPassword=findViewById(R.id.password_id);
        txtSignUp=findViewById(R.id.txtCreateAccount);
        txtSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this,createAccount.class);
                startActivity(intent);
            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String phone1=edPhone.getText().toString().trim();
                final String Password1 = edtPassword.getText().toString().trim();
                final ProgressDialog mDialog = new ProgressDialog(MainActivity.this);
                Paper.book().write(Prevalent.UserPhoneKey,phone1);
                mDialog.setMessage("Please waiting....");
                mDialog.show();

                reference= FirebaseDatabase.getInstance().getReference();
               reference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        if (dataSnapshot.child("Users").child(phone1).exists()){
                            User userd=dataSnapshot.child("Users").child(phone1).getValue(User.class);
                            if (userd.getPhone()!=null && phone1!=null && userd.getPhone().equals(phone1)){
                                if (userd.getPassword().equals(Password1)){
                                Toast.makeText(MainActivity.this, "Sign in Successfully as "+userd.getName(), Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(MainActivity.this,Home.class);
                                Prevalent.currentOnlineUser=userd;
                                 startActivity(intent);
                                    mDialog.dismiss();
                            }
                            else {
                                Toast.makeText(MainActivity.this, "Wrong Password", Toast.LENGTH_SHORT).show();
                            }
                            }
                        }
                        else{
                            mDialog.dismiss();
                            Toast.makeText(MainActivity.this,  phone1+" User not exist in Database "+county, Toast.LENGTH_SHORT).show();
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                        return;

                    }
                });
            }
        }
);
    }

}

