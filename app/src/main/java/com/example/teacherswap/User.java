package com.example.teacherswap;

public class User {
    String firstName;    String surname;    String tscNo;    String email;
    String phone;    String password;    String combination;
    String school;    String subCounty;    String county;
    String destinationSchool;   String destinationSubCnty;  String name;

    public User(){};
    public User(String firstName, String surname, String tscNo, String email, String phoneNo, String password, String combination,
                String school, String subCounty, String county, String destinationSchool, String destinationSubCnty) {
        this.firstName = firstName;
        this.surname = surname;
        this.tscNo = tscNo;
        this.email = email;
        this.phone = phoneNo;
        this.password = password;
        this.combination = combination;
        this.school = school;
        this.subCounty = subCounty;
        this.county = county;
        this.destinationSchool = destinationSchool;
        this.destinationSubCnty = destinationSubCnty;
    }

    public User(String tscNo, String combination, String school,
                String subCounty, String county, String destinationSchool, String destinationSubCnty, String name) {
        this.tscNo = tscNo;
        this.combination = combination;
        this.school = school;
        this.subCounty = subCounty;
        this.county = county;
        this.destinationSchool = destinationSchool;
        this.destinationSubCnty = destinationSubCnty;
        this.name = name;
    }

    public User(String county, String tscNo, String combination, String school, String subCounty) {
        this.tscNo = tscNo;
        this.combination = combination;
        this.school = school;
        this.subCounty = subCounty;
        this.county = county;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getTscNo() {
        return tscNo;
    }

    public void setTscNo(String tscNo) {
        this.tscNo = tscNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phoneNo) {
        this.phone = phoneNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCombination() {
        return combination;
    }

    public void setCombination(String combination) {
        this.combination = combination;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getSubCounty() {
        return subCounty;
    }

    public void setSubCounty(String subCounty) {
        this.subCounty = subCounty;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getDestinationSchool() {
        return destinationSchool;
    }

    public void setDestinationSchool(String destinationSchool) {
        this.destinationSchool = destinationSchool;
    }

    public String getDestinationSubCnty() {
        return destinationSubCnty;
    }

    public void setDestinationSubCnty(String destinationSubCnty) {
        this.destinationSubCnty = destinationSubCnty;
    }
}
