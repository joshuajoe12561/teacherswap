package com.example.teacherswap;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class createAccount extends AppCompatActivity {
private Button btnSignUp;
private EditText uFirstName,uLastName,uTscNo,uSchlName,uCounty,uSubCounty,uCombination,
        uPhone,uEmail,uPass,uConfPass,uDestinationSubCnty,uDestinationSchool;
private String usPass,usName, usTscNo,usSchlName,usCounty,usSubCounty,
        usCombination,usPhone,usEmail,usDestinationSchool,usDestinationSubCnty;
public static User currentUser;
private DatabaseReference reference;
private Spinner spinner;
    private static final String[] counties ={
            "Mombasa","Kwale","Kilifi","Tana River","Lamu","Taita–Taveta","Garissa","Wajir","Mandera",
            "Marsabit","Isiolo","Meru","Tharaka-Nithi","Embu","Kitui","Machakos","Makueni","Nyandarua",
            "Nyeri","Kirinyaga","Murang'a","Kiambu","Turkana","West Pokot","Samburu","Trans-Nzoia","Uasin Gishu",
            "Elgeyo-Marakwet","Nandi","Baringo","Laikipia","Nakuru","Narok","Kajiado","Kericho","Bomet",
            "Kakamega","Vihiga","Bungoma","Busia","Siaya","Kisumu","Homa Bay","Migori","Kisii",
            "Nyamira","Nairobi"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        reference=FirebaseDatabase.getInstance().getReference();

        uFirstName=findViewById(R.id.edFirstName);
        uLastName=findViewById(R.id.edSurname);
        uTscNo=findViewById(R.id.edTSCNo);
        uSchlName=findViewById(R.id.edSchlName);

        uSubCounty=findViewById(R.id.edSubCounty);
        uCombination=findViewById(R.id.edCombTaken);
        uPhone=findViewById(R.id.edPhoneNo);
        uEmail=findViewById(R.id.edEmail);
        uPass=findViewById(R.id.edPassword);
        uConfPass=findViewById(R.id.edPassConfirm);
        uDestinationSchool=findViewById(R.id.edDestSchl);
        uDestinationSubCnty=findViewById(R.id.edDestSubCnty);
        btnSignUp=findViewById(R.id.btnSignUp_id);
        spinner=findViewById(R.id.spna_id);


        //getValues of edTs

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(createAccount.this,
                android.R.layout.simple_spinner_dropdown_item,counties);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                usCounty=adapterView.getItemAtPosition(i).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Toast.makeText(createAccount.this, "please select your county", Toast.LENGTH_SHORT).show();

            }
        });
        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (uFirstName.getText().toString().isEmpty()||uLastName.getText().toString().isEmpty()){
                    uFirstName.setError("Enter  your First name and your Surname");
                }else {
                    usName=uFirstName.getText().toString()+" "+uLastName.getText().toString();
                }

                usTscNo=uTscNo.getText().toString();
                usSchlName=uSchlName.getText().toString();
                usSubCounty=uSubCounty.getText().toString();
                usCombination=uCombination.getText().toString();
                usPhone=uPhone.getText().toString();
                usEmail=uEmail.getText().toString();
                usDestinationSchool=uDestinationSchool.getText().toString();
                usDestinationSubCnty=uDestinationSubCnty.getText().toString();
                if (!uPass.getText().toString().equals(uConfPass.getText().toString())){
                    uPass.setError("Confirm password again !! password not equal confirm Password");
                }else{
                    usPass=uPass.getText().toString();
                }

                /*if (usName.isEmpty()||usPhone.isEmpty()||usSchlName.isEmpty()||usCounty.isEmpty()||usSubCounty.isEmpty()
                ||usCombination.isEmpty()||usEmail.isEmpty()||usTscNo.isEmpty()||usPass.isEmpty()||usDestinationSubCnty.isEmpty()
                ||usDestinationSchool.isEmpty()){
                    
                }*/
                HashMap map=new HashMap();
                        map.put("name",usName);
                        map.put("phone",usPhone);
                        map.put("school",usSchlName);
                        map.put("county",usCounty);
                        map.put("subCounty",usSubCounty);
                        map.put("combination",usCombination);
                        map.put("email",usEmail);
                        map.put("tscNo",usTscNo);
                        map.put("password",usPass);
                        map.put("destinationSubCnty",usDestinationSubCnty);
                        map.put("destinationSchool",usDestinationSchool);

                reference.child("Users").child(usPhone).updateChildren(map)
                        .addOnCompleteListener(new OnCompleteListener() {
                            @Override
                            public void onComplete(@NonNull Task task) {
                                if (task.isSuccessful()){

                                    Intent intent=new Intent(createAccount.this,MainActivity.class);
                                    Toast.makeText(createAccount.this, " Account creation successful..", Toast.LENGTH_SHORT).show();
                                    startActivity(intent);
                                }
                            }
                        });
            }
        });
    }
    }


