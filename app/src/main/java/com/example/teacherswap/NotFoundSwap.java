package com.example.teacherswap;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class NotFoundSwap extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_not_found_swap);
    }
}