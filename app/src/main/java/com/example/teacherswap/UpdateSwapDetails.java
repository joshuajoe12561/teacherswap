package com.example.teacherswap;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;

public class UpdateSwapDetails extends AppCompatActivity {

    private DatabaseReference reference;
    EditText nameEdt,tscNoEdt,schoolEdt,countyEdt,combinEdt,subCntEdt,destSubCntEdt,destSchlEdt;
    Button btnUpdate;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_details);

        nameEdt=findViewById(R.id.edtName_id);
        tscNoEdt=findViewById(R.id.edtTscno_id);
        schoolEdt=findViewById(R.id.edtSchl_id);
        countyEdt=findViewById(R.id.edtCounty_id);
        combinEdt=findViewById(R.id.edtCombntn_id);
        subCntEdt=findViewById(R.id.edtSubCounty_id);
        destSubCntEdt=findViewById(R.id.edtDestSubCnty_id);
        destSchlEdt=findViewById(R.id.edtDestSchl_id);
        btnUpdate=findViewById(R.id.update);

        reference= FirebaseDatabase.getInstance().getReference();

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String usName1=nameEdt.getText().toString();
                String ustscNoEdt1=tscNoEdt.getText().toString();
                String usSchool1=schoolEdt.getText().toString();
                String usCounty1=countyEdt.getText().toString();
                String usSubCounty1=subCntEdt.getText().toString();
                String usCombination1=combinEdt.getText().toString();
                String usDestinationSubCnty1=destSubCntEdt.getText().toString();
                String usDestinationSchool1=destSchlEdt.getText().toString();

                HashMap map=new HashMap();
                map.put("name",usName1);
                map.put("school",usSchool1);
                map.put("county",usCounty1);
                map.put("sub County",usSubCounty1);
                map.put("combination",usCombination1);
                map.put("tscNo",ustscNoEdt1);
                map.put("destinationSubCnty",usDestinationSubCnty1);
                map.put("destinationSchool",usDestinationSchool1);

                reference.child("Users").child(Prevalent.currentOnlineUser.getPhone()).updateChildren(map)
                        .addOnCompleteListener(new OnCompleteListener() {
                            @Override
                            public void onComplete(@NonNull Task task) {
                                if (task.isSuccessful()){
                                    Intent intent=new Intent(UpdateSwapDetails.this,Home.class);
                                    startActivity(intent);
                                    Toast.makeText(UpdateSwapDetails.this, " Account update successful..", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });

    }
    }
