package com.example.teacherswap;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

public class Home extends AppCompatActivity {

    private TabLayout emgtablayout;
    private ViewPager viewPager1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        emgtablayout= findViewById(R.id.userTablayout_id);
        viewPager1=findViewById(R.id.viewPager1_id);

        ViewPagerAdapter adapter=new ViewPagerAdapter(getSupportFragmentManager());
        adapter.AddFragment(new swapDetails(),"Swap Details");
        adapter.AddFragment(new Potential_Swap(),"Potential Swap");
        adapter.AddFragment(new searchSwap(),"Search");
        viewPager1.setAdapter(adapter);
        emgtablayout.setupWithViewPager(viewPager1);
        emgtablayout.getTabAt(0).setIcon(R.drawable.swapdetails_24dp);
        emgtablayout.getTabAt(1).setIcon(R.drawable.swap_horiz_black_24dp);
        emgtablayout.getTabAt(2).setIcon(R.drawable.search_black_24dp);
    }
}
