package com.example.teacherswap;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.teacherswap.adapters.SwapView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;




public class Potential_Swap extends Fragment {

    private RecyclerView recyclerView2;

    View v;

    @NonNull
    @Override
    public View onCreateView( LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
         v=inflater.inflate(R.layout.potential_swap, container, false);
        recyclerView2 = v.findViewById(R.id.recycler_menu);
        recyclerView2.setLayoutManager(new LinearLayoutManager(getActivity()));
        fetch();

        return v;
    }

    private void fetch(){

        final ArrayList<User> userList=new ArrayList<>();
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference().child("Users");
        Query query=dbRef.orderByChild("combination").equalTo(Prevalent.currentOnlineUser.getCombination());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    for (DataSnapshot childSnapshot: dataSnapshot.getChildren()) {
                    if (childSnapshot.child("subCounty").getValue().equals(Prevalent.currentOnlineUser.getDestinationSubCnty())){
                        User user=childSnapshot.getValue(User.class);

                        Log.d(this.getClass().getName(), user.combination);
                        userList.add(user);
                    }
                }
                SwapView swapAdapter = new SwapView(userList);
                swapAdapter.setOnEntryClickListener(new SwapView.OnEntryClickListener() {
                    @Override
                    public void onEntryClick(View view, int position) {
                        Intent intent=new Intent(getContext(),ProcessSwap.class);
                        startActivity(intent);

                    }
                });
                recyclerView2.setAdapter(swapAdapter);
            }else {
                Intent intent=new Intent(getContext(),NotFoundSwap.class);
                startActivity(intent);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

}
