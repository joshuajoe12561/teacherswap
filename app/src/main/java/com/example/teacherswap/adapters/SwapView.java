package com.example.teacherswap.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.teacherswap.R;
import com.example.teacherswap.User;

import java.util.ArrayList;

public class SwapView extends RecyclerView.Adapter<SwapView.SwapViewHolder> {
    public ArrayList<User> mCustomObjects;
    public SwapView(ArrayList<User> arrayList){
        mCustomObjects=arrayList;
    }
    @NonNull
    @Override
    public SwapViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.swap_layout, parent, false);
        return new SwapViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SwapViewHolder holder, int position) {
        User swapUser = mCustomObjects.get(position);
            holder.tvTscNo.setText(swapUser.getTscNo());
            holder.tvSchool.setText(swapUser.getSchool());
            holder.tvCombination.setText(swapUser.getCombination());
            holder.tvSubCounty.setText(swapUser.getSubCounty());
            holder.tvCounty.setText(swapUser.getCounty());
    }

    @Override
    public int getItemCount() {
        return mCustomObjects.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class  SwapViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView tvTscNo,tvSchool,tvCombination,tvSubCounty,tvCounty;
        public SwapViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvTscNo=itemView.findViewById(R.id.TscNo_ids);
            tvSchool=itemView.findViewById(R.id.school_ids);
            tvCombination=itemView.findViewById(R.id.combin_ids);
            tvSubCounty=itemView.findViewById(R.id.subCnty_ids);
            tvCounty=itemView.findViewById(R.id.countyids);
        }

        @Override
        public void onClick(View v) {
            if (mOnEntryClickListener != null){
                mOnEntryClickListener.onEntryClick(v,getLayoutPosition());
            }
        }
    }

    private  OnEntryClickListener mOnEntryClickListener;
    public interface OnEntryClickListener{
        void onEntryClick(View view, int position);
    }
    public void setOnEntryClickListener(OnEntryClickListener onEntryClickListener){
        mOnEntryClickListener = onEntryClickListener;
    }

}
