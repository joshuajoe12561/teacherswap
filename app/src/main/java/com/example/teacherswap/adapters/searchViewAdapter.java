package com.example.teacherswap.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.teacherswap.R;
import com.example.teacherswap.User;

import java.util.ArrayList;

public class searchViewAdapter extends RecyclerView.Adapter<searchViewAdapter.SearchViewHolder> {

    public ArrayList<User> mSearchResults;
    public searchViewAdapter(ArrayList<User> arrayList){
        mSearchResults=arrayList;
    }
    @NonNull
    @Override
    public SearchViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.search_result, parent, false);
            return new SearchViewHolder(view);

    }
private void setFadeAnimation(View view){
    AlphaAnimation anim=new AlphaAnimation(0.0f,1.0f);
    anim.setDuration(1000);
    view.startAnimation(anim);
}
    @Override
    public void onBindViewHolder(@NonNull SearchViewHolder holder, int position) {
        User n= mSearchResults.get(position);
        holder.tvCountys.setText(n.getCounty());
        holder.tvTscNo.setText(n.getTscNo());
        holder.tvCombination.setText(n.getCombination());
        holder.tvSchool.setText(n.getSchool());
        holder.tvSubCounty.setText(n.getSubCounty());
        setFadeAnimation(holder.itemView);
    }

    @Override
    public int getItemCount() {
        return mSearchResults.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class  SearchViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView tvCountys,tvTscNo,tvCombination,tvSchool,tvSubCounty;
        public SearchViewHolder(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvCountys=itemView.findViewById(R.id.searchingResult);
            tvTscNo=itemView.findViewById(R.id.resultTscNo);
            tvCombination=itemView.findViewById(R.id.resultCombination);
            tvSchool=itemView.findViewById(R.id.resultSchool);
            tvSubCounty=itemView.findViewById(R.id.resultSubCounty);
        }

        @Override
        public void onClick(View v) {
            if (mOnEntryClickListener != null){
                mOnEntryClickListener.onEntryClick(v,getLayoutPosition());
            }
        }
    }

    private SwapView.OnEntryClickListener mOnEntryClickListener;
    public interface OnEntryClickListener{
        void onEntryClick(View view, int position);
    }
    public void setOnEntryClickListener(SwapView.OnEntryClickListener onEntryClickListener){
        mOnEntryClickListener = onEntryClickListener;
    }
}
