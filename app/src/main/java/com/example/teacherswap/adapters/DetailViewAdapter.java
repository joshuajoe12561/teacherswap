package com.example.teacherswap.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.teacherswap.R;
import com.example.teacherswap.User;

import java.util.ArrayList;

public class DetailViewAdapter extends RecyclerView.Adapter<DetailViewAdapter.detailsViewHolders>{
    public ArrayList<User> userDetails;
    public DetailViewAdapter(ArrayList<User> arrayList){
        userDetails=arrayList;
    }

    @Override
    public detailsViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View vwd= LayoutInflater.from(parent.getContext())
                .inflate(R.layout.swap_detail_view,parent,false);
        return new detailsViewHolders(vwd);
    }

    @Override
    public void onBindViewHolder(@NonNull detailsViewHolders holder, int position) {
        User tuser = userDetails.get(position);
        holder.txtName.setText(tuser.getName());
        holder.txtTscNo.setText(tuser.getTscNo());
        holder.txtSchool.setText(tuser.getSchool());
        holder.txtCounty.setText(tuser.getCounty());
        holder.txtSubCounty.setText(tuser.getSubCounty());
        holder.txtCombination.setText(tuser.getCombination());
        holder.txtDestSubCnty.setText(tuser.getDestinationSubCnty());
        holder.txtDestSchool.setText(tuser.getDestinationSchool());
    }

    @Override
    public int getItemCount() {
        return userDetails.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private  OnEntryClickListener mOnEntryClickListener;

    public interface OnEntryClickListener{
        void onEntryClick(View view, int position);
    }
    public void setOnEntryClickListener(OnEntryClickListener onEntryClickListener) {
        mOnEntryClickListener=onEntryClickListener;
    }

    public class detailsViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{
            TextView txtName,txtTscNo,txtSchool,txtCounty,txtSubCounty,txtCombination,txtDestSubCnty,txtDestSchool;

            public detailsViewHolders(@NonNull View itemView) {
                super(itemView);
                itemView.setOnClickListener(this);
                txtName=itemView.findViewById(R.id.name_id);
                txtTscNo=itemView.findViewById(R.id.tscNo_id);
                txtSchool=itemView.findViewById(R.id.schl_id);
                txtCounty=itemView.findViewById(R.id.county_id);
                txtSubCounty=itemView.findViewById(R.id.subCounty_id);
                txtCombination=itemView.findViewById(R.id.combntn_id);
                txtDestSubCnty=itemView.findViewById(R.id.destSubCnty_id);
                txtDestSchool=itemView.findViewById(R.id.destSchl_id);
            }

            @Override
            public void onClick(View v) {
                if (mOnEntryClickListener != null){
                    mOnEntryClickListener.onEntryClick(v,getLayoutPosition());
                }
            }
        }
    }
