package com.example.teacherswap;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.teacherswap.adapters.SwapView;
import com.example.teacherswap.adapters.searchViewAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class searchSwap extends Fragment {
    private RecyclerView recyclerViewing;
    private View v;
    private Button btnSearch;
    private EditText edtSearch;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v=inflater.inflate(R.layout.search, container, false);
        recyclerViewing=v.findViewById(R.id.result_recyclerView);
        recyclerViewing.setHasFixedSize(true);
        recyclerViewing.setLayoutManager(new LinearLayoutManager(getActivity()));
        btnSearch=v.findViewById(R.id.searchButton);
        edtSearch=v.findViewById(R.id.searchWords);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fetch();
            }
        });

        return v;
    }
    private void fetch(){
        String searchWord=edtSearch.getText().toString();

        final ArrayList<User> userList=new ArrayList<>();
        Query query = FirebaseDatabase.getInstance()
                .getReference()
                .child("Users")
                .orderByChild("county")
                .equalTo(searchWord);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {

                    for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                        if (childSnapshot.child("combination").getValue().equals(Prevalent.currentOnlineUser.getCombination())) {
                            User user = childSnapshot.getValue(User.class);
                            if (user != null) {
                                Log.d(this.getClass().getName(), " user  not  empty");
                            }
                            Log.d(this.getClass().getName(), user.combination);
                            userList.add(user);
                        }
                    }
                    searchViewAdapter searchAdapter = new searchViewAdapter(userList);
                    searchAdapter.setOnEntryClickListener(new SwapView.OnEntryClickListener() {
                        @Override
                        public void onEntryClick(View view, int position) {
                            Intent intent = new Intent(getContext(), ProcessSwap.class);
                            startActivity(intent);
                        }
                    });
                    recyclerViewing.setAdapter(searchAdapter);

                }else {
                    Intent intent = new Intent(getContext(), NotFoundSwap.class);
                    startActivity(intent);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
