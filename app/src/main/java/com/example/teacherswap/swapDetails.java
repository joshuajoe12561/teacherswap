package com.example.teacherswap;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.teacherswap.adapters.DetailViewAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class swapDetails extends Fragment {
    private RecyclerView recyclerView;
    private View vw;


    @Nullable
    @Override
    public View onCreateView( LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {

        vw=inflater.inflate(R.layout.swap_details,container,false);
        recyclerView=vw.findViewById(R.id.detailsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        fetch();
        return vw;
    }
    private void fetch() {
        final ArrayList<User> userData = new ArrayList<>();
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference().child("Users");
        Query query = dbRef.orderByChild("phone").equalTo(Prevalent.currentOnlineUser.getPhone());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    User user = childSnapshot.getValue(User.class);
                    if (user==null){
                        Log.d(this.getClass().getName(), " user is empty");
                    }
                    Log.d(this.getClass().getName(), user.name);
                    userData.add(user);

                }
                DetailViewAdapter detailViewAdapter = new DetailViewAdapter(userData);
                detailViewAdapter.setOnEntryClickListener(new DetailViewAdapter.OnEntryClickListener() {
                    @Override
                    public void onEntryClick(View view, int position) {
                        Intent intent = new Intent(getContext(), UpdateSwapDetails.class);
                        startActivity(intent);
                    }
                });
                recyclerView.setAdapter(detailViewAdapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
